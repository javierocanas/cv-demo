import React from 'react'
import _ from 'lodash';
import { callApi } from '../server/api.js'
import SearchBar from './search_bar'
import EpisodeList from './episode_list';
import EpisodeDetail from './episode_detail';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      seasons: [],
      selectedSeason: null,
      selectedSeasonNumber: 1,
      episodes: [],
      selectedEpisode: null };
    callApi('544242').then((r) => {
      this.setState({
        seasons: r.seasons,
        selectedSeason: r.seasons[0],
        episodes: r.seasons[0].episodes
      })
    });
  }
  episodeSearch(term) {
    if (!term) {
      this.setState({ episodes: this.state.seasons[this.state.selectedSeasonNumber - 1].episodes });
    }
    let episodes = [];
    this.state.episodes.forEach((e) => {
      const string = e.title_episode.toLowerCase();
      const substring = term.toLowerCase();
      if (string.includes(substring)) {
        episodes.push(e);
      };
    });
    this.setState({ episodes: episodes });
  }
  selectSeason(seasonNumer) {
    this.setState({
      episodes: this.state.seasons[seasonNumber].episodes
    })
  }
  render () {
    const selectSeason = !!this.state.seasons && this.state.seasons.map((s,k) =>
      <option key={k} value={k}>{s.title}</option>
    )
    const episodeSearch = _.debounce((term) => { this.episodeSearch(term) }, 300);
    const imgUrl = !!this.state.selectedSeason && this.state.selectedSeason.image_background;
    const divStyle = {
      color: 'blue',
      backgroundImage: 'url(' + imgUrl + ')',
    };
    return (
      <div className="fluid-container cv-bg" style={divStyle}>
        <div className="row content">
          <div className="col-9">
            <EpisodeDetail episode={this.state.selectedEpisode} />
          </div>
          <div className="col-3 episodes-list">
            <span>
              <select onChange={event => this.setState({ 
                selectedSeason: this.state.seasons[Number(event.target.value)],
                selectedSeasonNumber: Number(event.target.value) + 1,
                episodes: this.state.seasons[Number(event.target.value)].episodes,
                selectedEpisode: null
              })}>
                {selectSeason}
              </select>
            </span>
            <SearchBar onSearchTermChange={episodeSearch} />
            <EpisodeList
              onEpisodeSelect={selectedEpisode => this.setState({selectedEpisode}) }
              episodes={this.state.episodes} />
          </div>
        </div>
      </div>
    )
  }
}
