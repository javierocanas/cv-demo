import React from 'react';

const EpisodeListItem = ({episode, onEpisodeSelect}) => {
  const imageUrl = episode.image_small;

  return (
    <li onClick={() => onEpisodeSelect(episode)}>
      <div className="media">
        <div className="media-left">
          <img className="media-object" src={imageUrl} />
        </div>
        <div className="media-body">
          <div className="media-heading">{episode.title_episode}</div>
        </div>
      </div>
    </li>
  );
};

export default EpisodeListItem;