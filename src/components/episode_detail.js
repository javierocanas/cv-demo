import React from 'react';

const EpisodeDetail = ({episode}) => {
  if (!episode) {
    return <div className="episode-detail">
      <div className="details">
        {/*This is hardcoded for now*/}
        <h2>Breaking Bad</h2>
      </div>
    </div>
  }
  const episodeId = episode.id;
  
  return (
    <div className="episode-detail">
      <div className="details">
        <h2>{episode.title_episode}</h2>
        <div>{episode.description_large}</div>
      </div>
    </div>
  );
};

export default EpisodeDetail;