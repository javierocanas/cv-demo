import React from 'react';
import EpisodeListItem from './episode_list_item';

const EpisodeList = (props) => {
  const episodeItems = props.episodes.map((episode) => {
    return (
      <EpisodeListItem
        onEpisodeSelect={props.onEpisodeSelect}
        key={episode.id}
        episode={episode} />
    );
  });

  return (
    <ul className="list-group">
      {episodeItems}
    </ul>
  );
};

export default EpisodeList;
