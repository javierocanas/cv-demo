import fetch from 'node-fetch';

export async function callApi(_id) {
  const res = await fetch(`https://mfwkweb-api.clarovideo.net/services/content/serie?api_version=v5.4&authpn=webclient&authpt=tfg1h3j4k6fd7&format=json&region=mexico&device_id=web&device_category=web&device_model=web&device_type=web&device_manufacturer=generic&HKS=q12fva9pjnhauhd4jg0d02e0t1&group_id=${_id}`);
  const json = await res.json();
  return json.response;
}
